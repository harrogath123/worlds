USE classicmodels;

BEGIN;

-- Modification de classicmodels.customers
ALTER TABLE classicmodels.customers
ADD COLUMN temp_country CHAR(10);

-- Liaison de classicmodels.customers.temp_country à world.country
UPDATE classicmodels.customers
SET temp_country = (SELECT world.country.Code FROM world.country WHERE world.country.Name = classicmodels.customers.country);

-- Suppression de l'ancienne colonne country
ALTER TABLE classicmodels.customers
DROP COLUMN country;

-- Renommer la nouvelle colonne en country
ALTER TABLE classicmodels.customers
CHANGE COLUMN temp_country country CHAR(10);

-- Modification de classicmodels.offices
ALTER TABLE classicmodels.offices
ADD COLUMN temp_country CHAR(25);

-- Liaison de classicmodels.offices.temp_country à world.country
UPDATE classicmodels.offices
SET temp_country = (SELECT world.country.Code FROM world.country WHERE world.country.Name = classicmodels.offices.country);

-- Suppression de l'ancienne colonne country
ALTER TABLE classicmodels.offices
DROP COLUMN country;

-- Modification de classicmodels.orders
ALTER TABLE classicmodels.orders
MODIFY COLUMN status ENUM('Shipped', 'Resolved', 'Cancelled', 'On Hold', 'Disputed', 'In Process');

-- Ajout de classicmodels.customers.language
ALTER TABLE classicmodels.customers
ADD COLUMN language VARCHAR(25);

COMMIT;








