# Présentation de l'exercice

Explications

La base de données présente trois tables (city, country et country language )

Ces dernières sont connectées ensemble par les clés étrangères et les tables proposées.

Elles présentent les pays du monde, les villes et les capitales ainsi que les langues parlées à travers le monde comme l'anglais, l'espagnol, le français etc.

Le but étant de regarder la base de données, la connecter, vérifier les données et de voir si ces dernières sont utilisables en sql et si les commandes sql fonctionnent.

![modèle de base de données](/home/benjamin/world/MLDS.png "Modèle de base de données")

## Différences entre les différents langages

Différence entre var et varchar:

**CHAR :**

* `CHAR`
  est un type de données de caractères de longueur fixe.
* Lorsque vous définissez une
  colonne en tant que `CHAR`, vous devez
  spécifier une longueur fixe pour la colonne.
* Si les données réelles insérées dans la colonne sont plus
  courtes que la longueur spécifiée, elles sont remplies avec des
  espaces pour atteindre la longueur fixe.

**VARCHAR :**

* `VARCHAR`
  est un type de données de caractères de longueur variable.
* Lorsque vous définissez une
  colonne en tant que `VARCHAR`, vous
  spécifiez une longueur maximale pour la colonne, mais les données
  réelles peuvent varier en longueur jusqu'à ce maximum.
* `VARCHAR` ne remplit pas les
  données avec des espaces ; il ne stocke que les caractères réels.

`CHAR` a une longueur fixe, tandis que
`VARCHAR` a une longueur variable.

* `CHAR`
  remplit les données avec des espaces pour atteindre la longueur
  fixe, alors que `VARCHAR` ne le fait pas.
* `CHAR`
  peut être plus efficace pour certains types de requêtes (par
  exemple, lorsque la longueur des données est généralement proche
  de la longueur définie), mais il peut utiliser plus d'espace de
  stockage pour des données de longueur variable.
* `VARCHAR` est plus flexible pour
  stocker des données de longueur variable et n'utilise de l'espace
  que pour la longueur réelle des données.

Varchar est
conseillé dans une base de données pour l’économie de stockage.

Définition des
references:

Une référence, ou clé étrangère, est une colonne ou un
ensemble de colonnes dans une table de base de données relationnelle
qui fait référence à la clé primaire (ou à une colonne unique)
d'une autre table.

* Elle établit un lien logique
  entre les données des deux tables, permettant de représenter des
  relations entre les entités de la base de données.
* La colonne référente dans la
  table liée est généralement appelée clé primaire, et la colonne
  qui fait référence à cette clé primaire est appelée clé
  étrangère.
* L'utilisation de références garantit que chaque valeur dans
  la colonne référente de la clé étrangère existe dans la colonne
  référencée, assurant ainsi l'intégrité référentielle.

En résumé, les
références en SQL sont des mécanismes qui facilitent
l'établissement de liens entre les données de différentes tables,
contribuant ainsi à la cohérence, à l'intégrité et à la logique
des relations au sein d'une base de données relationnelle.

Les contraintes et
définition:

En bases de données,
les contraintes (constraints en anglais) sont des règles définies
sur les données d'une table afin de garantir l'intégrité, la
cohérence et la validité des données stockées. Les contraintes
spécifient des conditions auxquelles les données doivent obéir
pour respecter la logique métier de l'application. Voici les
principales contraintes que l'on retrouve généralement dans les
bases de données relationnelles :

Ces contraintes sont
essentielles pour garantir la qualité des données, empêcher les
incohérences, et faciliter la maintenance et la compréhension de la
structure de la base de données. En SQL, les contraintes sont
généralement spécifiées lors de la création de la table, mais
elles peuvent également être ajoutées ou modifiées ultérieurement
pour adapter la structure de la base de données aux évolutions des
besoins.

1. **Création de la table "city" :**
   * La première ligne commence par créer une nouvelle table appelée "city".
2. **Colonnes de la table "city" :**
   * Les lignes suivantes définissent les différentes colonnes de la table "city" :
     * `ID` : Un identifiant unique pour chaque enregistrement, auto-incrémenté à chaque nouvel enregistrement.
     * `Name` : Le nom de la ville, une chaîne de caractères d'une longueur maximale de 35 caractères.
     * `CountryCode` : Le code du pays auquel la ville appartient, une chaîne de trois caractères.
     * `District` : Le district ou la région de la ville, une chaîne de caractères d'une longueur maximale de 20 caractères.
     * `Population` : La population de la ville, représentée par un entier.
3. **Clé primaire :**
   * La ligne `PRIMARY KEY (`ID `)` définit la clé primaire de la table, qui utilise la colonne `ID` comme identifiant unique.
4. **Index :**
   * La ligne `KEY `CountryCode ` (`CountryCode `)` crée un index sur la colonne `CountryCode`. Cela peut améliorer les performances lors de la recherche ou du tri des enregistrements basés sur cette colonne.
5. **Clé étrangère :**
   * La ligne `CONSTRAINT `city_ibfk_1 ` FOREIGN KEY (`CountryCode `) REFERENCES `country ` (`Code `)` définit une clé étrangère sur la colonne `CountryCode`. Cela signifie que la valeur dans la colonne `CountryCode` doit correspondre à une valeur existante dans la colonne `Code` de la table `country`. En d'autres termes, cela établit une relation entre les tables "city" et "country" basée sur le code du pays.

En résumé, ce code crée une table "city" avec des colonnes spécifiques, une clé primaire sur la colonne "ID", un index sur la colonne "CountryCode", et une clé étrangère reliant la colonne "CountryCode" à la colonne "Code" de la table "country".

1. **Création de la table "country" :**
   * La première ligne commence par créer une nouvelle table appelée "country".
2. **Colonnes de la table "country" :**
   * Les lignes suivantes définissent les différentes colonnes de la table "country" :
     * `Code` : Le code du pays, une chaîne de trois caractères, utilisé comme clé primaire.
     * `Name` : Le nom du pays, une chaîne de caractères d'une longueur maximale de 52 caractères.
     * `Continent` : Le continent auquel le pays appartient, représenté par une énumération. Les options possibles sont Asie, Europe, Amérique du Nord, Afrique, Océanie, Antarctique, Amérique du Sud.
     * `Region` : La région géographique du pays, une chaîne de caractères d'une longueur maximale de 26 caractères.
     * `SurfaceArea` : La superficie du pays, représentée par un nombre décimal avec deux chiffres après la virgule.
     * `IndepYear` : L'année d'indépendance du pays, un entier court (smallint) ou NULL s'il n'y a pas d'année d'indépendance spécifiée.
     * `Population` : La population du pays, représentée par un entier.
     * `LifeExpectancy` : L'espérance de vie du pays, représentée par un nombre décimal avec une décimale.
     * `GNP` : Le produit national brut du pays, représenté par un nombre décimal avec deux chiffres après la virgule.
     * `GNPOld` : Le produit national brut ancien du pays, représenté par un nombre décimal avec deux chiffres après la virgule.
     * `LocalName` : Le nom local du pays, une chaîne de caractères d'une longueur maximale de 45 caractères.
     * `GovernmentForm` : La forme de gouvernement du pays, une chaîne de caractères d'une longueur maximale de 45 caractères.
     * `HeadOfState` : Le chef d'État du pays, une chaîne de caractères d'une longueur maximale de 60 caractères ou NULL s'il n'y a pas de chef d'État spécifié.
     * `Capital` : L'ID de la ville qui sert de capitale du pays, un entier ou NULL s'il n'y a pas de capitale spécifiée.
     * `Code2` : Le code à deux lettres du pays, une chaîne de deux caractères.
3. **Clé primaire :**
   * La ligne `PRIMARY KEY (`Code `)` définit la clé primaire de la table, qui utilise la colonne `Code` comme identifiant unique.
4. **Options de la table :**
   * La ligne `ENGINE=InnoDB DEFAULT CHARSET=utf8mb4` spécifie le moteur de stockage InnoDB et le jeu de caractères utf8mb4 pour la table "country".

En résumé, ce code crée une table "country" avec des colonnes spécifiques, une clé primaire sur la colonne "Code" et des caractéristiques de stockage spécifiées. Cette table peut être utilisée pour stocker des informations sur différents pays du monde.

1. **Création de la table "countrylanguage" :**

   * La première ligne commence par créer une nouvelle table appelée "countrylanguage".
2. **Colonnes de la table "countrylanguage" :**

   * Les lignes suivantes définissent les différentes colonnes de la table "countrylanguage" :
     * `CountryCode` : Le code du pays auquel la langue est associée, une chaîne de trois caractères, et cette colonne fait partie de la clé primaire.
     * `Language` : La langue parlée dans le pays, une chaîne de caractères d'une longueur maximale de 30 caractères, et cette colonne fait également partie de la clé primaire.
     * `IsOfficial` : Un indicateur de langue officielle, représenté par une énumération. Les options possibles sont 'T' pour vrai (official) et 'F' pour faux (non-official).
     * `Percentage` : Le pourcentage de la population du pays qui parle cette langue, représenté par un nombre décimal avec une décimale.
3. **Clé primaire :**

   * La ligne `PRIMARY KEY (`CountryCode `,`Language `)` définit la clé primaire de la table, composée des colonnes `CountryCode` et `Language`. Cela garantit l'unicité des combinaisons pays-langue.
4. 
5. **Options de la table :**

   * La ligne `ENGINE=InnoDB DEFAULT CHARSET=utf8mb4` spécifie le moteur de stockage InnoDB et le jeu de caractères utf8mb4 pour la table "countrylanguage".
6. **Index :**

   * La ligne `KEY `idx_CountryCode `(`CountryCode `)` crée un index sur la colonne `CountryCode`. Cela peut améliorer les performances lors de la recherche ou du tri des enregistrements basés sur cette colonne.
7. **Clé étrangère :**

   * La ligne `CONSTRAINT `city_ibfk_1 `FOREIGN KEY (`CountryCode `) REFERENCES `country `(`Code `)` définit une clé étrangère sur la colonne `CountryCode`. Cela signifie que la valeur dans la colonne `CountryCode` de la table "city" doit correspondre à une valeur existante dans la colonne `Code` de la table "country". En d'autres termes, cela établit une relation entre les tables "city" et "country" basée sur le code du pays.

En résumé, ce code crée une table "countrylanguage" pour stocker des informations sur les langues parlées dans différents pays, avec des colonnes spécifiques, une clé primaire composée, un index et une clé étrangère.

### Détails des normes utlisées et autres

[Lien vers la norme ISO_3166-2]()(https://fr.wikipedia.org/wiki/ISO_3166-2)

Norme pour la dénomination des villes dans le monde en français.

[Même lien mais en anglais]() (https://en.wikipedia.org/wiki/ISO_3166-2)

La base de données doit certainement dater de 2001 car le président Argentin était au pouvoir de 1999 à 2001 selon cette base de données.

#### Réponses aux questions.

#Question 1

* Liste des type de gouvernement avec nombre de pays pour chaque.

[Histoire des gouvernements](https://fr.wikipedia.org/wiki/Liste_de_formes_de_gouvernements)

[Les différents types de gouvernements](https://fr.wikipedia.org/wiki/R%C3%A9gime_politique)

```
SELECT GovernmentForm, COUNT(Code) AS NumberOfCountries
FROM country
GROUP BY GovernmentForm;
```

```GovernmentForm


+----------------------------------------------+-------------------+
| Nonmetropolitan Territory of The Netherlands |                 2 |
| Islamic Emirate                              |                 1 |
| Republic                                     |               123 |
| Dependent Territory of the UK                |                12 |
| Parliamentary Coprincipality                 |                 1 |
| Emirate Federation                           |                 1 |
| Federal Republic                             |                14 |
| US Territory                                 |                 3 |
| Co-administrated                             |                 1 |
| Nonmetropolitan Territory of France          |                 4 |
| Constitutional Monarchy                      |                29 |
| Constitutional Monarchy, Federation          |                 4 |
| Monarchy (Emirate)                           |                 1 |
| Monarchy (Sultanate)                         |                 2 |
| Monarchy                                     |                 5 |
| Dependent Territory of Norway                |                 2 |
| Territory of Australia                       |                 4 |
| Federation                                   |                 1 |
| People'sRepublic                             |                 1 |
| Nonmetropolitan Territory of New Zealand     |                 3 |
| Socialistic Republic                         |                 3 |
| Occupied by Marocco                          |                 1 |
| Part of Denmark                              |                 2 |
| Overseas Department of France                |                 4 |
| Special Administrative Region of China       |                 2 |
| Islamic Republic                             |                 2 |
| Constitutional Monarchy (Emirate)            |                 1 |
| Socialistic State                            |                 1 |
| Commonwealth of the US                       |                 2 |
| Territorial Collectivity of France           |                 2 |
| Autonomous Area                              |                 1 |
| Administrated by the UN                      |                 1 |
| Dependent Territory of the US                |                 1 |
| Independent Church State                     |                 1 |
| Parlementary Monarchy                        |                 1 |
+----------------------------------------------+-------------------+
```

#Deuxième question:

* Pourquoi `countrylanguage.IsOfficial` utilise un enum et pas un bool ?

Pour répondre à la deuxième question concernant la table `countrylanguage` avec la colonne `IsOfficial`

Il pourrait utiliser un booléen, mais le modèle a choisi d'utiliser un
enum pour prendre en compte d'éventuelles nuances dans le statut de
langue officielle.

En fonction des langues employées à travers le monde, il existe des langues principales et des langues secondaires comme le provençal ou l'Occitan en France.

C'est pourquoi le modèle de données a choisi d'utiliser un enum pour représenter ces différentes nuances.

#Question 3

* D’apres la BDD, combien de personne dans le monde parle anglais ?

```
SELECT
    SUM(c.Population) AS TotalEnglishSpeakers
FROM
    countrylanguage cl
JOIN
    country c ON cl.CountryCode = c.Code
WHERE
    cl.Language = 'English' AND cl.IsOfficial = 'T';

```

Le nombre total de personnes qui parlent anglais à travers le monde.

[Les personnes qui parlent anglais](https://overtheword.com/combien-de-langues-parlees-dans-le-monde-en-2023/))

```
+----------------------+
| TotalEnglishSpeakers |
+----------------------+
|            459158800 |
+----------------------+
```

#Question 4:

* Faire la liste des langues avec le nombre de locuteur, de la plus parlée à la moins parlée.

```
SELECT
    Language,
    SUM(Population * Percentage / 100) AS TotalSpeakers
FROM
    countrylanguage cl
JOIN
    country c ON cl.CountryCode = c.Code
GROUP BY
    Language
ORDER BY
    TotalSpeakers DESC;

```

Les personnes les plus nombreuses:

[Les pays avec les nombres d&#39;habitant](https://www.populationpyramid.net/fr/population-par-pays/2023/)

[Les langues parlées dans le top 10](https://overtheword.com/combien-de-langues-parlees-dans-le-monde-en-2023/)

```
+---------------------------+------------------+
| Language                  | TotalSpeakers    |
+---------------------------+------------------+
| Chinese                   | 1191843539.00000 |
| Hindi                     |  405633070.00000 |
| Spanish                   |  355029462.00000 |
| English                   |  347077867.30000 |
| Arabic                    |  233839238.70000 |
| Bengali                   |  209304719.00000 |
| Portuguese                |  177595269.40000 |
| Russian                   |  160807561.30000 |
| Japanese                  |  126814108.00000 |
| Punjabi                   |  104025371.00000 |
| German                    |   92133584.70000 |
| Javanese                  |   83570158.00000 |
| Telugu                    |   79065636.00000 |
| Marathi                   |   75019094.00000 |
| Korean                    |   72291372.00000 |
| Vietnamese                |   70616218.00000 |
| French                    |   69980880.40000 |
| Tamil                     |   68691536.00000 |
| Urdu                      |   63589470.00000 |
| Turkish                   |   62205657.20000 |
| Italian                   |   59864483.20000 |
| Gujarati                  |   48655776.00000 |
| Malay                     |   41517994.00000 |
| Kannada                   |   39532818.00000 |
| Polish                    |   39525035.10000 |
| Ukrainian                 |   36593408.00000 |
| Malajalam                 |   36491832.00000 |
| Thai                      |   33996960.00000 |
| Sunda                     |   33512906.00000 |
| Orija                     |   33450846.00000 |
| Pashto                    |   32404553.00000 |
| Burmese                   |   31471590.00000 |
| Persian                   |   31124734.00000 |
| Hausa                     |   29225396.00000 |
| Joruba                    |   24868874.00000 |
| Ful                       |   23704612.00000 |
| Romanian                  |   23457777.30000 |
| Uzbek                     |   22535760.00000 |
| Pilipino                  |   22258331.00000 |
| Dutch                     |   21388666.00000 |
| Ibo                       |   20182586.00000 |
| Lao                       |   20167307.00000 |
| Oromo                     |   19395150.00000 |
| Kurdish                   |   19062628.00000 |
| Azerbaijani               |   19014911.00000 |
| Amhara                    |   18769500.00000 |
| Sindhi                    |   18464994.00000 |
| Zhuang                    |   17885812.00000 |
| Cebuano                   |   17700311.00000 |
| Serbo-Croatian            |   16762504.70000 |
| Malagasy                  |   15800413.00000 |
| Asami                     |   15527778.00000 |
| Saraiki                   |   15335334.00000 |
| Akan                      |   15026888.00000 |
| Min                       |   14844752.00000 |
| Berberi                   |   13817820.00000 |
| Rwanda                    |   13750258.00000 |
| Nepali                    |   12799872.00000 |
| Somali                    |   12770598.00000 |
| Hungarian                 |   12652201.90000 |
| Khmer                     |   11810683.00000 |
| Greek                     |   11638803.60000 |
| Mantšu                    |   11498022.00000 |
| Kongo                     |   11480181.00000 |
| Singali                   |   11352681.00000 |
| Hui                       |   10220464.00000 |
| Shona                     |    9892055.00000 |
| Miao                      |    9661394.00000 |
| Zulu                      |    9508689.00000 |
| Luba                      |    9297720.00000 |
| Kazakh                    |    9258230.00000 |
| Mossi                     |    9185870.00000 |
| Madura                    |    9120601.00000 |
| Czech                     |    8362000.80000 |
| Swedish                   |    8255142.60000 |
| Haiti Creole              |    8222000.00000 |
| Belorussian               |    7671574.00000 |
| Uighur                    |    7665348.00000 |
| Yi                        |    7665348.00000 |
| Dari                      |    7293120.00000 |
| Mongolian                 |    7207888.00000 |
| Xhosa                     |    7146729.00000 |
| Nyamwesi                  |    7072087.00000 |
| Ilocano                   |    7064931.00000 |
| Bulgariana                |    7036276.80000 |
| Mongo                     |    6973290.00000 |
| Hiligaynon                |    6912997.00000 |
| Catalan                   |    6690841.30000 |
| Canton Chinese            |    6628268.00000 |
| Kirundi                   |    6567795.00000 |
| Balochi                   |    6456116.00000 |
| Tigrinja                  |    6395030.00000 |
| Tujia                     |    6387790.00000 |
| Chichewa                  |    6369275.00000 |
| Kikuyu                    |    6286720.00000 |
| Ibibio                    |    6244336.00000 |
| Afrikaans                 |    5937881.00000 |
| Ketšua                    |    5768437.00000 |
| Albaniana                 |    5664230.80000 |
| Tatar                     |    5525159.00000 |
| Makua                     |    5471040.00000 |
| Tibetan                   |    5110232.00000 |
| Minangkabau               |    5090568.00000 |
| Kanuri                    |    5043866.00000 |
| Slovak                    |    5024431.50000 |
| Finnish                   |    5016543.10000 |
| Danish                    |    5008464.00000 |
| Tadzhik                   |    4956520.00000 |
| Turkmenian                |    4934965.00000 |
| Wolof                     |    4901011.00000 |
| Ovimbundu                 |    4790616.00000 |
| Batakki                   |    4666354.00000 |
| Bugi                      |    4666354.00000 |
| Creole English            |    4518135.00000 |
| Tswana                    |    4495147.00000 |
| Mandarin Chinese          |    4479132.00000 |
| Malinke                   |    4459198.00000 |
| Norwegian                 |    4386528.00000 |
| Bicol                     |    4330119.00000 |
| Tsonga                    |    4176531.00000 |
| Luhya                     |    4151040.00000 |
| Hebrew                    |    4050068.00000 |
| Armenian                  |    4024652.00000 |
| Zande                     |    3947124.00000 |
| Ganda                     |    3941818.00000 |
| Shan                      |    3876935.00000 |
| Luo                       |    3850240.00000 |
| Banja                     |    3817926.00000 |
| Fang                      |    3794797.00000 |
| Papuan Languages          |    3792451.00000 |
| Hindko                    |    3755592.00000 |
| Mixed Languages           |    3690092.00000 |
| Edo                       |    3679698.00000 |
| Northsotho                |    3674307.00000 |
| Bali                      |    3605819.00000 |
| Gilaki                    |    3588206.00000 |
| Bambara                   |    3572412.00000 |
| Georgiana                 |    3562056.00000 |
| Lomwe                     |    3545240.00000 |
| Ewe                       |    3479156.00000 |
| Dinka                     |    3391350.00000 |
| Kamba                     |    3368960.00000 |
| Kalenjin                  |    3248640.00000 |
| Southsotho                |    3068652.00000 |
| Lithuanian                |    3047066.40000 |
| Ngala and Bangi           |    2995932.00000 |
| Swahili                   |    2949496.00000 |
| Gurage                    |    2940555.00000 |
| Luri                      |    2911186.00000 |
| Waray-waray               |    2886746.00000 |
| Maithili                  |    2847670.00000 |
| Karen                     |    2827882.00000 |
| Bamileke-bamum            |    2805810.00000 |
| Kirgiz                    |    2805303.00000 |
| Mbundu                    |    2781648.00000 |
| Creole French             |    2767307.00000 |
| Bemba                     |    2723193.00000 |
| Tiv                       |    2564638.00000 |
| Hakka                     |    2556672.00000 |
| Dong                      |    2555116.00000 |
| Puyi                      |    2555116.00000 |
| Galecian                  |    2524268.80000 |
| Arabic-French             |    2521118.00000 |
| Ndebele                   |    2517119.00000 |
| Mazandarani               |    2437272.00000 |
| Fon                       |    2426606.00000 |
| Nubian Languages          |    2388690.00000 |
| Sara                      |    2350687.00000 |
| Nkole                     |    2330246.00000 |
| Hehet                     |    2312673.00000 |
| Pampango                  |    2279010.00000 |
| Songhai-zerma             |    2274760.00000 |
| Guaraní                   |    2212225.00000 |
| Hassaniya                 |    2181390.00000 |
| Rakhine                   |    2052495.00000 |
| Bhojpuri                  |    2039088.00000 |
| Ijo                       |    2007108.00000 |
| Sidamo                    |    2002080.00000 |
| Haya                      |    1977503.00000 |
| Makonde                   |    1977503.00000 |
| Rundi                     |    1962852.00000 |
| Tamashek                  |    1936002.00000 |
| Swazi                     |    1915617.00000 |
| Teke                      |    1903797.00000 |
| Slovene                   |    1895003.40000 |
| Beja                      |    1887360.00000 |
| Brahui                    |    1877796.00000 |
| Sena                      |    1849920.00000 |
| Gusii                     |    1834880.00000 |
| Sotho                     |    1830050.00000 |
| Nyakusa                   |    1809918.00000 |
| Kiga                      |    1807574.00000 |
| Soga                      |    1785796.00000 |
| Bura                      |    1784096.00000 |
| Náhuatl                   |    1779858.00000 |
| Kru                       |    1779618.00000 |
| Walaita                   |    1751820.00000 |
| Gur                       |    1729962.00000 |
| Mende                     |    1689192.00000 |
| Meru                      |    1654400.00000 |
| Duala                     |    1644265.00000 |
| Chaga and Pare            |    1642333.00000 |
| Luguru                    |    1642333.00000 |
| Nyanja                    |    1621340.00000 |
| Macedonian                |    1615524.60000 |
| Ga-adangme                |    1576536.00000 |
| Sardinian                 |    1557360.00000 |
| Temne                     |    1543572.00000 |
| Gurma                     |    1504791.00000 |
| Chokwe                    |    1470648.00000 |
| Araucan                   |    1460256.00000 |
| Nuer                      |    1445010.00000 |
| Nyika                     |    1443840.00000 |
| Yao                       |    1442100.00000 |
| Shambala                  |    1441231.00000 |
| Malenasian Languages      |    1438620.00000 |
| Tho                       |    1436976.00000 |
| Pangasinan                |    1367406.00000 |
| Senufo and Minianka       |    1348080.00000 |
| Latvian                   |    1335734.20000 |
| Moravian                  |    1325874.90000 |
| Chuvash                   |    1322406.00000 |
| Gogo                      |    1307163.00000 |
| Teso                      |    1306680.00000 |
| Tharu                     |    1292220.00000 |
| Lango                     |    1284902.00000 |
| Soninke                   |    1271881.00000 |
| Southern Slavic Languages |    1265504.00000 |
| Tigre                     |    1220450.00000 |
| Muong                     |    1197480.00000 |
| Boa                       |    1188042.00000 |
| Serer                     |    1185125.00000 |
| Tswa                      |    1180800.00000 |
| Ha                        |    1173095.00000 |
| Tamang                    |    1172570.00000 |
| Bakhtyari                 |    1150934.00000 |
| Quiché                    |    1149885.00000 |
| [South]Mande              |    1138522.00000 |
| Chuabo                    |    1121760.00000 |
| Tikar                     |    1116290.00000 |
| Tagalog                   |    1113428.00000 |
| Mon                       |    1094664.00000 |
| Yucatec                   |    1087691.00000 |
| Maguindanao               |    1063538.00000 |
| Dzongkha                  |    1062000.00000 |
| Bashkir                   |    1028538.00000 |
| Lugbara                   |    1023566.00000 |
| Cakchiquel                |    1013265.00000 |
| Tonga                     |    1008590.00000 |
| Chin                      |    1003442.00000 |
| Crioulo                   |     996393.00000 |
| Maranao                   |     987571.00000 |
| Gisu                      |     980010.00000 |
| Acholi                    |     958232.00000 |
| Kpelle                    |     956810.00000 |
| Estonian                  |     950140.20000 |
| Romani                    |     943952.00000 |
| Aimará                    |     932809.00000 |
| Mon-khmer                 |     896445.00000 |
| Venda                     |     888294.00000 |
| Newari                    |     885410.00000 |
| Chechen                   |     881604.00000 |
| Mayo-kebbi                |     879865.00000 |
| Nung                      |     878152.00000 |
| Ovambo                    |     875082.00000 |
| Gbaya                     |     860370.00000 |
| Mandara                   |     859845.00000 |
| Banda                     |     849525.00000 |
| Susu                      |     817300.00000 |
| Songhai                   |     775146.00000 |
| Maka                      |     739165.00000 |
| Bari                      |     737250.00000 |
| Mordva                    |     734670.00000 |
| Ngoni                     |     731975.00000 |
| Ronga                     |     728160.00000 |
| Luimbe-nganguela          |     695412.00000 |
| Nyaneka-nkhumbi           |     695412.00000 |
| Friuli                    |     692160.00000 |
| Marendje                  |     688800.00000 |
| Kanem-bornu               |     688590.00000 |
| Adja                      |     676767.00000 |
| Kuy                       |     675389.00000 |
| Ouaddai                   |     665637.00000 |
| Kabyé                     |     638802.00000 |
| Kachin                    |     638554.00000 |
| Basque                    |     631067.20000 |
| Iban                      |     622832.00000 |
| Fur                       |     619290.00000 |
| Diola                     |     594110.00000 |
| Mixtec                    |     593286.00000 |
| Zapotec                   |     593286.00000 |
| Masana                    |     588315.00000 |
| Avarian                   |     587736.00000 |
| Mari                      |     587736.00000 |
| Fries                     |     586968.00000 |
| Lozi                      |     586816.00000 |
| Man                       |     558824.00000 |
| Kekchí                    |     557865.00000 |
| Kymri                     |     536610.60000 |
| Mandjia                   |     535020.00000 |
| Aizo                      |     530439.00000 |
| Bariba                    |     530439.00000 |
| Chewa                     |     522633.00000 |
| Chakma                    |     516620.00000 |
| Hadjarai                  |     512617.00000 |
| Chilluk                   |     501330.00000 |
| Tandjile                  |     497315.00000 |
| Karakalpak                |     486360.00000 |
| Masai                     |     481280.00000 |
| Watyi                     |     476787.00000 |
| Gorane                    |     474362.00000 |
| Luvale                    |     463608.00000 |
| Indian Languages          |     454765.00000 |
| Comorian                  |     453072.00000 |
| Kissi                     |     445800.00000 |
| Lotuko                    |     442350.00000 |
| Udmur                     |     440802.00000 |
| Bassa                     |     432098.00000 |
| Turkana                   |     421120.00000 |
| Busansi                   |     417795.00000 |
| Fijian                    |     415036.00000 |
| Somba                     |     408499.00000 |
| Limba                     |     402882.00000 |
| Otomí                     |     395524.00000 |
| Nsenga                    |     394267.00000 |
| Afar                      |     387574.00000 |
| Yalunka                   |     380506.00000 |
| Dagara                    |     370047.00000 |
| Maltese                   |     364231.60000 |
| Loma                      |     353822.00000 |
| Sranantonga               |     337770.00000 |
| Mboshi                    |     335502.00000 |
| Mbete                     |     310452.00000 |
| Dyula                     |     310362.00000 |
| Ambo                      |     309072.00000 |
| Luchazi                   |     309072.00000 |
| Mam                       |     307395.00000 |
| Arabic-French-English     |     306752.00000 |
| Dhivehi                   |     286000.00000 |
| Lao-Soung                 |     282516.00000 |
| Grebo                     |     280706.00000 |
| Luxembourgish             |     280590.80000 |
| Ngbaka                    |     271125.00000 |
| Tšam                      |     268032.00000 |
| Icelandic                 |     267003.00000 |
| Papiamento                |     266055.00000 |
| Ane                       |     263853.00000 |
| Kotokoli                  |     263853.00000 |
| Marma                     |     258310.00000 |
| Bajan                     |     256770.00000 |
| Moba                      |     249966.00000 |
| Gio                       |     249166.00000 |
| Kono-vai                  |     247554.00000 |
| Dusun                     |     244684.00000 |
| Mbum                      |     231360.00000 |
| Mano                      |     227088.00000 |
| Nama                      |     214024.00000 |
| Punu-sira-nzebi           |     209646.00000 |
| Naudemba                  |     189789.00000 |
| Philippene Languages      |     188156.00000 |
| Assyrian                  |     184920.00000 |
| Bullom-sherbro            |     184452.00000 |
| Kayah                     |     182444.00000 |
| Chibcha                   |     181353.00000 |
| Mpongwe                   |     178996.00000 |
| Lezgian                   |     177882.00000 |
| Balante                   |     177098.00000 |
| Kavango                   |     167422.00000 |
| Maori                     |     166066.00000 |
| Kuranko                   |     165036.00000 |
| Guaymí                    |     151368.00000 |
| Samoan                    |     147108.00000 |
| Hadareb                   |     146300.00000 |
| Tukulor                   |     144180.00000 |
| Gagauzi                   |     140160.00000 |
| Herero                    |     138080.00000 |
| Ami                       |     133536.00000 |
| Garo                      |     129155.00000 |
| Khasi                     |     129155.00000 |
| Santhali                  |     129155.00000 |
| Tripuri                   |     129155.00000 |
| Fukien                    |     128858.00000 |
| Osseetti                  |     119232.00000 |
| Bilin                     |     115500.00000 |
| Saho                      |     115500.00000 |
| Tahitian                  |     109040.00000 |
| Bislama                   |     107540.00000 |
| Garifuna                  |     100693.00000 |
| Tongan                    |      99425.00000 |
| Goajiro                   |      96680.00000 |
| Chiu chau                 |      94948.00000 |
| Malay-English             |      94464.00000 |
| Miskito                   |      94154.00000 |
| Samoan-English            |      93600.00000 |
| San                       |      89564.00000 |
| Atayal                    |      89024.00000 |
| Punu                      |      85347.00000 |
| Kiribati                  |      85135.00000 |
| Abhyasi                   |      84456.00000 |
| Caprivi                   |      81122.00000 |
| Sango                     |      76518.00000 |
| Comorian-French           |      74562.00000 |
| Chamorro                  |      73128.00000 |
| Dorbet                    |      71874.00000 |
| Seselwa                   |      70301.00000 |
| Paiwan                    |      66768.00000 |
| Mahoré                    |      62431.00000 |
| Marshallese               |      61952.00000 |
| Caribbean                 |      61263.00000 |
| Irish                     |      60401.60000 |
| Gaeli                     |      59623.40000 |
| Mandyako                  |      59437.00000 |
| Czech and Moravian        |      59385.70000 |
| Cuna                      |      57120.00000 |
| Arawakan                  |      54375.00000 |
| Circassian                |      50830.00000 |
| Bajad                     |      50578.00000 |
| Trukese                   |      49504.00000 |
| Greenlandic               |      49000.00000 |
| Buryat                    |      45254.00000 |
| Faroese                   |      43000.00000 |
| Romansh                   |      42962.40000 |
| Polynesian Languages      |      41696.00000 |
| Silesiana                 |      41112.40000 |
| Khoekhoe                  |      40550.00000 |
| Bubi                      |      39411.00000 |
| Dariganga                 |      37268.00000 |
| Ukrainian and Russian     |      32392.20000 |
| Zenaga                    |      32040.00000 |
| Comorian-madagassi        |      31790.00000 |
| Eskimo Languages          |      31147.00000 |
| Rapa nui                  |      30422.00000 |
| Pohnpei                   |      28322.00000 |
| Warrau                    |      24170.00000 |
| Maya Languages            |      23136.00000 |
| Embera                    |      17136.00000 |
| Palau                     |      15618.00000 |
| Tuvalu                    |      12535.00000 |
| Sumo                      |      10148.00000 |
| Comorian-Arabic           |       9248.00000 |
| Mortlock                  |       9044.00000 |
| Kosrean                   |       8687.00000 |
| Yap                       |       6902.00000 |
| Nauru                     |       6900.00000 |
| Monegasque                |       5474.00000 |
| Wolea                     |       4403.00000 |
| Carolinian                |       3744.00000 |
| Comorian-Swahili          |       2890.00000 |
| Sinaberberi               |          0.00000 |
| Saame                     |          0.00000 |
| Ainu                      |          0.00000 |
| Niue                      |          0.00000 |
| Pitcairnese               |          0.00000 |
| Nahua                     |          0.00000 |
| Tokelau                   |          0.00000 |
| Futuna                    |          0.00000 |
| Wallis                    |          0.00000 |
| Soqutri                   |          0.00000 |
+---------------------------+------------------+
```

#Question 5:

* En quelle unité est exprimée la surface des pays ?

[superficie des pays du monde et indice de mensuration des pays et territoires à travers le monde](https://fr.wikipedia.org/wiki/Liste_des_pays_et_territoires_par_superficie)

Dans le cas de la base de données cette dernière utilise les M² pour les pays car la valeur par défaut 0.00 est utilisée tandis que pour les miles c'est comme ceci: 0.0000

#Question 6:

* Faire la liste des pays qui ont plus 10 000 000 d’hab. avec leur capitale et le % de la population qui habite dans la capitale.
* [Liste complète des capitales et des nombres d&#39;habitants](https://www.techno-science.net/glossaire-definition/Liste-des-capitales-du-monde-par-population.html)
* [Liste des populations par pays jusqu&#39;en 2020 sauf pour L&#39;Inde et le Kosovo](https://fr.vikidia.org/wiki/Liste_des_pays_par_population)

```
SELECT
    c.Name AS Country,
    ci.Name AS Capital,
    c.Population AS TotalPopulation,
    ci.Population AS CapitalPopulation,
    (ci.Population / c.Population) * 100 AS CapitalPercentage
FROM
    country c
JOIN
    city ci ON c.Capital = ci.ID
WHERE
    c.Population > 10000000
ORDER BY
    c.Population DESC;

```

```+---------------------------------------+---------------------+-----------------+-------------------+-------------------+
```+---------------------------------------+---------------------+-----------------+-------------------+-------------------+
| Country                               | Capital             | TotalPopulation | CapitalPopulation | CapitalPercentage |
+---------------------------------------+---------------------+-----------------+-------------------+-------------------+
| China                                 | Peking              |      1277558000 |           7472000 |            0.5849 |
| India                                 | New Delhi           |      1013662000 |            301297 |            0.0297 |
| United States                         | Washington          |       278357000 |            572059 |            0.2055 |
| Indonesia                             | Jakarta             |       212107000 |           9604900 |            4.5283 |
| Brazil                                | Brasília            |       170115000 |           1969868 |            1.1580 |
| Pakistan                              | Islamabad           |       156483000 |            524500 |            0.3352 |
| Russian Federation                    | Moscow              |       146934000 |           8389200 |            5.7095 |
| Bangladesh                            | Dhaka               |       129155000 |           3612850 |            2.7973 |
| Japan                                 | Tokyo               |       126714000 |           7980230 |            6.2978 |
| Nigeria                               | Abuja               |       111506000 |            350100 |            0.3140 |
| Mexico                                | Ciudad de México    |        98881000 |           8591309 |            8.6885 |
| Germany                               | Berlin              |        82164700 |           3386667 |            4.1218 |
| Vietnam                               | Hanoi               |        79832000 |           1410000 |            1.7662 |
| Philippines                           | Manila              |        75967000 |           1581082 |            2.0813 |
| Egypt                                 | Cairo               |        68470000 |           6789479 |            9.9160 |
| Iran                                  | Teheran             |        67702000 |           6758845 |            9.9832 |
| Turkey                                | Ankara              |        66591000 |           3038159 |            4.5624 |
| Ethiopia                              | Addis Abeba         |        62565000 |           2495000 |            3.9879 |
| Thailand                              | Bangkok             |        61399000 |           6320174 |           10.2936 |
| United Kingdom                        | London              |        59623400 |           7285000 |           12.2184 |
| France                                | Paris               |        59225700 |           2125246 |            3.5884 |
| Italy                                 | Roma                |        57680000 |           2643581 |            4.5832 |
| Congo, The Democratic Republic of the | Kinshasa            |        51654000 |           5064000 |            9.8037 |
| Ukraine                               | Kyiv                |        50456000 |           2624000 |            5.2006 |
| South Korea                           | Seoul               |        46844000 |           9981619 |           21.3082 |
| Myanmar                               | Rangoon (Yangon)    |        45611000 |           3361700 |            7.3704 |
| Colombia                              | Santafé de Bogotá   |        42321000 |           6260862 |           14.7937 |
| South Africa                          | Pretoria            |        40377000 |            658630 |            1.6312 |
| Spain                                 | Madrid              |        39441700 |           2879052 |            7.2995 |
| Poland                                | Warszawa            |        38653600 |           1615369 |            4.1791 |
| Argentina                             | Buenos Aires        |        37032000 |           2982146 |            8.0529 |
| Tanzania                              | Dodoma              |        33517000 |            189000 |            0.5639 |
| Algeria                               | Alger               |        31471000 |           2168000 |            6.8889 |
| Canada                                | Ottawa              |        31147000 |            335277 |            1.0764 |
| Kenya                                 | Nairobi             |        30080000 |           2290000 |            7.6130 |
| Sudan                                 | Khartum             |        29490000 |            947483 |            3.2129 |
| Morocco                               | Rabat               |        28351000 |            623457 |            2.1991 |
| Peru                                  | Lima                |        25662000 |           6464693 |           25.1917 |
| Uzbekistan                            | Toskent             |        24318000 |           2117500 |            8.7075 |
| Venezuela                             | Caracas             |        24170000 |           1975294 |            8.1725 |
| North Korea                           | Pyongyang           |        24039000 |           2484000 |           10.3332 |
| Nepal                                 | Kathmandu           |        23930000 |            591835 |            2.4732 |
| Iraq                                  | Baghdad             |        23115000 |           4336000 |           18.7584 |
| Afghanistan                           | Kabul               |        22720000 |           1780000 |            7.8345 |
| Romania                               | Bucuresti           |        22455500 |           2016131 |            8.9783 |
| Taiwan                                | Taipei              |        22256000 |           2641312 |           11.8679 |
| Malaysia                              | Kuala Lumpur        |        22244000 |           1297526 |            5.8332 |
| Uganda                                | Kampala             |        21778000 |            890800 |            4.0904 |
| Saudi Arabia                          | Riyadh              |        21607000 |           3324000 |           15.3839 |
| Ghana                                 | Accra               |        20212000 |           1070000 |            5.2939 |
| Mozambique                            | Maputo              |        19680000 |           1018938 |            5.1775 |
| Australia                             | Canberra            |        18886000 |            322723 |            1.7088 |
| Sri Lanka                             | Colombo             |        18827000 |            645000 |            3.4259 |
| Yemen                                 | Sanaa               |        18112000 |            503600 |            2.7805 |
| Kazakstan                             | Astana              |        16223000 |            311200 |            1.9183 |
| Syria                                 | Damascus            |        16125000 |           1347000 |            8.3535 |
| Madagascar                            | Antananarivo        |        15942000 |            675669 |            4.2383 |
| Netherlands                           | Amsterdam           |        15864000 |            731200 |            4.6092 |
| Chile                                 | Santiago de Chile   |        15211000 |           4703954 |           30.9247 |
| Cameroon                              | Yaoundé             |        15085000 |           1372800 |            9.1004 |
| Côte d’Ivoire                         | Yamoussoukro        |        14786000 |            130000 |            0.8792 |
| Angola                                | Luanda              |        12878000 |           2022000 |           15.7012 |
| Ecuador                               | Quito               |        12646000 |           1573458 |           12.4423 |
| Burkina Faso                          | Ouagadougou         |        11937000 |            824000 |            6.9029 |
| Zimbabwe                              | Harare              |        11669000 |           1410000 |           12.0833 |
| Guatemala                             | Ciudad de Guatemala |        11385000 |            823301 |            7.2315 |
| Mali                                  | Bamako              |        11234000 |            809552 |            7.2063 |
| Cuba                                  | La Habana           |        11201000 |           2256000 |           20.1411 |
| Cambodia                              | Phnom Penh          |        11168000 |            570155 |            5.1053 |
| Malawi                                | Lilongwe            |        10925000 |            435964 |            3.9905 |
| Niger                                 | Niamey              |        10730000 |            420000 |            3.9143 |
| Yugoslavia                            | Beograd             |        10640000 |           1204000 |           11.3158 |
| Greece                                | Athenai             |        10545700 |            772072 |            7.3212 |
| Czech Republic                        | Praha               |        10278100 |           1181126 |           11.4917 |
| Belgium                               | Bruxelles [Brussel] |        10239000 |            133859 |            1.3073 |
| Belarus                               | Minsk               |        10236000 |           1674000 |           16.3540 |
| Somalia                               | Mogadishu           |        10097000 |            997000 |            9.8742 |
| Hungary                               | Budapest            |        10043200 |           1811552 |           18.0376 |
+---------------------------------------+---------------------+-----------------+-------------------+-------------------+
```

Les capitales et les pourcentages d'habitants entre les pays et les capitales.

#Question 7:

* Liste des 10 pays avec le plus fort taux de croissance entre n et n-1 avec le % de croissance
* [La croissance des pays dans le monde pour les années 2024 et 2025](https://www.oecd.org/perspectives-economiques/novembre-2023/)(https://www.oecd.org/perspectives-economiques/novembre-2023/)

```
SELECT
    Name AS Country,
    GNP AS GNP_N,
    GNPOld AS GNP_N_1,
    ((GNP - GNPOld) / GNPOld) * 100 AS GrowthPercentage
FROM
    country
ORDER BY
    GrowthPercentage DESC
LIMIT 10;

```

```+---------------------------------------+-----------+-----------+------------------+
| Congo, The Democratic Republic of the |   6964.00 |   2474.00 |       181.487470 |
| Turkmenistan                          |   4397.00 |   2000.00 |       119.850000 |
| Tajikistan                            |   1990.00 |   1056.00 |        88.446970 |
| Estonia                               |   5328.00 |   3371.00 |        58.053990 |
| Albania                               |   3205.00 |   2500.00 |        28.200000 |
| Suriname                              |    870.00 |    706.00 |        23.229462 |
| Iran                                  | 195746.00 | 160151.00 |        22.225899 |
| Bulgaria                              |  12178.00 |  10169.00 |        19.756122 |
| Honduras                              |   5333.00 |   4697.00 |        13.540558 |
| Latvia                                |   6398.00 |   5639.00 |        13.459833 |
+---------------------------------------+-----------+-----------+------------------+
```

Les dix pays ayant la croissance la plus importante.

#Question 8:

* Liste des pays plurilingues avec pour chacun le nombre de langues parlées.

```
SELECT
    c.Name AS Country,
    COUNT(cl.Language) AS NumberOfLanguages
FROM
    country c
JOIN
    countrylanguage cl ON c.Code = cl.CountryCode
GROUP BY
    c.Code
HAVING
    COUNT(cl.Language) > 1
ORDER BY
    NumberOfLanguages DESC;

```

Le plus de langues parlées par pays.

[Les langues parlées au Canada](https://www150.statcan.gc.ca/n1/daily-quotidien/220817/dq220817a-fra.htm)(https://www150.statcan.gc.ca/n1/daily-quotidien/220817/dq220817a-fra.htm)

[Les langues au Canada et les explications](https://fr.wikipedia.org/wiki/Langues_au_Canada)(https://fr.wikipedia.org/wiki/Langues_au_Canada)

[Les langues parlées aux Etats-Unis en priorité](https://www.itc-france-traduction.com/blog/langues-plus-parlees-aux-etats-unis/)(https://www.itc-france-traduction.com/blog/langues-plus-parlees-aux-etats-unis/)

```|
+---------------------------------------+-------------------+
| Canada                                |                12 |
| China                                 |                12 |
| India                                 |                12 |
| Russian Federation                    |                12 |
| United States                         |                12 |
| Tanzania                              |                11 |
| South Africa                          |                11 |
| Congo, The Democratic Republic of the |                10 |
| Iran                                  |                10 |
| Kenya                                 |                10 |
| Mozambique                            |                10 |
| Nigeria                               |                10 |
| Philippines                           |                10 |
| Sudan                                 |                10 |
| Uganda                                |                10 |
| Angola                                |                 9 |
| Indonesia                             |                 9 |
| Vietnam                               |                 9 |
| Australia                             |                 8 |
| Austria                               |                 8 |
| Cameroon                              |                 8 |
| Czech Republic                        |                 8 |
| Italy                                 |                 8 |
| Liberia                               |                 8 |
| Myanmar                               |                 8 |
| Namibia                               |                 8 |
| Pakistan                              |                 8 |
| Sierra Leone                          |                 8 |
| Chad                                  |                 8 |
| Togo                                  |                 8 |
| Benin                                 |                 7 |
| Bangladesh                            |                 7 |
| Denmark                               |                 7 |
| Ethiopia                              |                 7 |
| Guinea                                |                 7 |
| Kyrgyzstan                            |                 7 |
| Nepal                                 |                 7 |
| Ukraine                               |                 7 |
| Belgium                               |                 6 |
| Burkina Faso                          |                 6 |
| Central African Republic              |                 6 |
| Congo                                 |                 6 |
| Germany                               |                 6 |
| Eritrea                               |                 6 |
| France                                |                 6 |
| Micronesia, Federated States of       |                 6 |
| Georgia                               |                 6 |
| Ghana                                 |                 6 |
| Guinea-Bissau                         |                 6 |
| Hungary                               |                 6 |
| Japan                                 |                 6 |
| Kazakstan                             |                 6 |
| Latvia                                |                 6 |
| Mexico                                |                 6 |
| Mali                                  |                 6 |
| Mongolia                              |                 6 |
| Northern Mariana Islands              |                 6 |
| Mauritania                            |                 6 |
| Mauritius                             |                 6 |
| Malaysia                              |                 6 |
| Panama                                |                 6 |
| Romania                               |                 6 |
| Senegal                               |                 6 |
| Sweden                                |                 6 |
| Thailand                              |                 6 |
| Taiwan                                |                 6 |
| Uzbekistan                            |                 6 |
| Yugoslavia                            |                 6 |
| Zambia                                |                 6 |
| Afghanistan                           |                 5 |
| Brazil                                |                 5 |
| Botswana                              |                 5 |
| Côte d’Ivoire                         |                 5 |
| Colombia                              |                 5 |
| Comoros                               |                 5 |
| Estonia                               |                 5 |
| Finland                               |                 5 |
| Gambia                                |                 5 |
| Guatemala                             |                 5 |
| Guam                                  |                 5 |
| Hong Kong                             |                 5 |
| Iraq                                  |                 5 |
| Lithuania                             |                 5 |
| Luxembourg                            |                 5 |
| Moldova                               |                 5 |
| Macedonia                             |                 5 |
| Niger                                 |                 5 |
| Norway                                |                 5 |
| Nauru                                 |                 5 |
| Réunion                               |                 5 |
| Slovakia                              |                 5 |
| Aruba                                 |                 4 |
| Andorra                               |                 4 |
| Azerbaijan                            |                 4 |
| Bulgaria                              |                 4 |
| Belarus                               |                 4 |
| Belize                                |                 4 |
| Bolivia                               |                 4 |
| Brunei                                |                 4 |
| Switzerland                           |                 4 |
| Chile                                 |                 4 |
| Costa Rica                            |                 4 |
| Spain                                 |                 4 |
| Gabon                                 |                 4 |
| Honduras                              |                 4 |
| Cambodia                              |                 4 |
| Laos                                  |                 4 |
| Macao                                 |                 4 |
| Monaco                                |                 4 |
| Malawi                                |                 4 |
| Nicaragua                             |                 4 |
| Netherlands                           |                 4 |
| Palau                                 |                 4 |
| Poland                                |                 4 |
| Paraguay                              |                 4 |
| Turkmenistan                          |                 4 |
| Zimbabwe                              |                 4 |
| Albania                               |                 3 |
| Netherlands Antilles                  |                 3 |
| Argentina                             |                 3 |
| American Samoa                        |                 3 |
| Burundi                               |                 3 |
| Bhutan                                |                 3 |
| Djibouti                              |                 3 |
| United Kingdom                        |                 3 |
| Guyana                                |                 3 |
| Israel                                |                 3 |
| Jordan                                |                 3 |
| Lebanon                               |                 3 |
| Liechtenstein                         |                 3 |
| Sri Lanka                             |                 3 |
| Lesotho                               |                 3 |
| Mayotte                               |                 3 |
| New Caledonia                         |                 3 |
| Peru                                  |                 3 |
| French Polynesia                      |                 3 |
| Singapore                             |                 3 |
| Solomon Islands                       |                 3 |
| Slovenia                              |                 3 |
| Seychelles                            |                 3 |
| Tajikistan                            |                 3 |
| Trinidad and Tobago                   |                 3 |
| Tunisia                               |                 3 |
| Turkey                                |                 3 |
| Tuvalu                                |                 3 |
| Venezuela                             |                 3 |
| Virgin Islands, U.S.                  |                 3 |
| Vanuatu                               |                 3 |
| Samoa                                 |                 3 |
| United Arab Emirates                  |                 2 |
| Armenia                               |                 2 |
| Antigua and Barbuda                   |                 2 |
| Bahrain                               |                 2 |
| Bahamas                               |                 2 |
| Barbados                              |                 2 |
| Cocos (Keeling) Islands               |                 2 |
| Cook Islands                          |                 2 |
| Cape Verde                            |                 2 |
| Christmas Island                      |                 2 |
| Cyprus                                |                 2 |
| Dominica                              |                 2 |
| Dominican Republic                    |                 2 |
| Algeria                               |                 2 |
| Ecuador                               |                 2 |
| Egypt                                 |                 2 |
| Fiji Islands                          |                 2 |
| Faroe Islands                         |                 2 |
| Gibraltar                             |                 2 |
| Guadeloupe                            |                 2 |
| Equatorial Guinea                     |                 2 |
| Greece                                |                 2 |
| Greenland                             |                 2 |
| French Guiana                         |                 2 |
| Croatia                               |                 2 |
| Haiti                                 |                 2 |
| Ireland                               |                 2 |
| Iceland                               |                 2 |
| Jamaica                               |                 2 |
| Kiribati                              |                 2 |
| Saint Kitts and Nevis                 |                 2 |
| South Korea                           |                 2 |
| Kuwait                                |                 2 |
| Libyan Arab Jamahiriya                |                 2 |
| Saint Lucia                           |                 2 |
| Morocco                               |                 2 |
| Madagascar                            |                 2 |
| Maldives                              |                 2 |
| Marshall Islands                      |                 2 |
| Malta                                 |                 2 |
| Martinique                            |                 2 |
| Niue                                  |                 2 |
| New Zealand                           |                 2 |
| Oman                                  |                 2 |
| Papua New Guinea                      |                 2 |
| Puerto Rico                           |                 2 |
| North Korea                           |                 2 |
| Palestine                             |                 2 |
| Qatar                                 |                 2 |
| Rwanda                                |                 2 |
| Svalbard and Jan Mayen                |                 2 |
| El Salvador                           |                 2 |
| Somalia                               |                 2 |
| Sao Tome and Principe                 |                 2 |
| Suriname                              |                 2 |
| Swaziland                             |                 2 |
| Syria                                 |                 2 |
| Tokelau                               |                 2 |
| East Timor                            |                 2 |
| Tonga                                 |                 2 |
| Saint Vincent and the Grenadines      |                 2 |
| Wallis and Futuna                     |                 2 |
| Yemen                                 |                 2 |
+---------------------------------------+-------------------+
```

#Question 9:

* Liste des pays avec plusieurs langues officielles, le nombre de langues officielle et le nombre de langues du pays.
* [Les langues officielles en Afrique Du Sud](https://fr.wikipedia.org/wiki/Langues_en_Afrique_du_Sud)
* [Les langues officielles en Suisse](https://www.eda.admin.ch/aboutswitzerland/fr/home/gesellschaft/sprachen/die-sprachen---fakten-und-zahlen.html)(https://www.eda.admin.ch/aboutswitzerland/fr/home/gesellschaft/sprachen/die-sprachen---fakten-und-zahlen.html)

```
SELECT
    c.Name AS Country,
    COUNT(DISTINCT cl.Language) AS NumberOfOfficialLanguages,
    COUNT(cl.Language) AS TotalLanguages
FROM
    country c
JOIN
    countrylanguage cl ON c.Code = cl.CountryCode
WHERE
    cl.IsOfficial = 'T'
GROUP BY
    c.Code
HAVING
    COUNT(DISTINCT cl.Language) > 1
ORDER BY
    NumberOfOfficialLanguages DESC;

```

Le nombre de langues parlées et de langues officielles.

```|
+----------------------+---------------------------+----------------+
| South Africa         |                         4 |              4 |
| Switzerland          |                         4 |              4 |
| Luxembourg           |                         3 |              3 |
| Vanuatu              |                         3 |              3 |
| Singapore            |                         3 |              3 |
| Peru                 |                         3 |              3 |
| Bolivia              |                         3 |              3 |
| Belgium              |                         3 |              3 |
| Somalia              |                         2 |              2 |
| Nauru                |                         2 |              2 |
| Palau                |                         2 |              2 |
| Paraguay             |                         2 |              2 |
| Romania              |                         2 |              2 |
| Rwanda               |                         2 |              2 |
| Burundi              |                         2 |              2 |
| Malta                |                         2 |              2 |
| Seychelles           |                         2 |              2 |
| Togo                 |                         2 |              2 |
| Tonga                |                         2 |              2 |
| Tuvalu               |                         2 |              2 |
| American Samoa       |                         2 |              2 |
| Samoa                |                         2 |              2 |
| Netherlands Antilles |                         2 |              2 |
| Marshall Islands     |                         2 |              2 |
| Madagascar           |                         2 |              2 |
| Afghanistan          |                         2 |              2 |
| Lesotho              |                         2 |              2 |
| Sri Lanka            |                         2 |              2 |
| Kyrgyzstan           |                         2 |              2 |
| Israel               |                         2 |              2 |
| Ireland              |                         2 |              2 |
| Guam                 |                         2 |              2 |
| Greenland            |                         2 |              2 |
| Faroe Islands        |                         2 |              2 |
| Finland              |                         2 |              2 |
| Cyprus               |                         2 |              2 |
| Belarus              |                         2 |              2 |
| Canada               |                         2 |              2 |
+----------------------+---------------------------+----------------+
```

Question 10:

* Liste des langues parlées en France avec le %

[Les différentes langues parlées en France](https://www.cia-france.fr/blog/culture-traditions-francaises/dialectes-francais)

L'Italien, L'Espagnol, Le Portugais, l'Arabe et le Turque sont des langues parlées car la France est un territoire d'accueil et d'immigration, notamment la main-d'oeuvre Portugaise, Espagnole etc.

* ```
  WITH LanguagePercentages AS (
      SELECT
          cl.Language,
          ROUND(cl.Percentage / SUM(cl.Percentage) OVER () * 100, 2) AS NormalizedPercentage
      FROM
          countrylanguage cl
      JOIN
          country c ON cl.CountryCode = c.Code
      WHERE
          cl.CountryCode = 'FRA'
  )
  SELECT * FROM LanguagePercentages;


  SELECT
      Language,
      ROUND(NormalizedPercentage * 100 / SUM(NormalizedPercentage) OVER (), 2) AS AdjustedPercentage
  FROM
      LanguagePercentages;

  ```

Le pourcentage de population en France.

[le pourcentage de population en France](https://www.insee.fr/fr/outil-interactif/5367857/tableau/20_DEM/21_POP)

[les populations en fonction des régions de France](https://www.ined.fr/fr/tout-savoir-population/chiffres/france/structure-population/regions/)


```
+------------+--------------------+
| Language   | AdjustedPercentage |
+------------+--------------------+
| Arabic     |               2.54 |
| French     |              95.01 |
| Italian    |               0.41 |
| Portuguese |               1.22 |
| Spanish    |               0.41 |
| Turkish    |               0.41 |
+------------+--------------------+
```

#Question 11:

* Pareil en chine.
* [Les langues parlées en Chine](https://fr.babbel.com/fr/magazine/le-guide-des-langues-chinoises)(https://fr.babbel.com/fr/magazine/le-guide-des-langues-chinoises)
* [la population en Chine et son évolution depuis 1960](https://perspective.usherbrooke.ca/bilan/servlet/BMTendanceStatPays?codePays=CHN&codeStat=SP.POP.TOTL)

```
WITH LanguagePercentages AS (
    SELECT
        cl.Language,
        ROUND(cl.Percentage / SUM(cl.Percentage) OVER () * 100, 2) AS NormalizedPercentage
    FROM
        countrylanguage cl
    JOIN
        country c ON cl.CountryCode = c.Code
    WHERE
        cl.CountryCode = 'CHN'
)

SELECT * FROM LanguagePercentages;

SELECT
    Language,
    ROUND(NormalizedPercentage * 100 / SUM(NormalizedPercentage) OVER (), 2) AS AdjustedPercentage
FROM
    LanguagePercentages;

```

Pourcentages de population en Chine.

```
+-----------+--------------------+
| Language  | AdjustedPercentage |
+-----------+--------------------+
| Chinese   |              93.20 |
| Dong      |               0.20 |
| Hui       |               0.81 |
| Mantšu    |               0.91 |
| Miao      |               0.71 |
| Mongolian |               0.41 |
| Puyi      |               0.20 |
| Tibetan   |               0.41 |
| Tujia     |               0.51 |
| Uighur    |               0.61 |
| Yi        |               0.61 |
| Zhuang    |               1.42 |
+-----------+--------------------+
```

#Question 12:

* Pareil aux états unis.

Les Etats-Unis sont un état ouvert vers les différentes communautés et les langues pratiquées comme l'Espagnol avec la proximité avec le Mexique et les pays d'Amérique Latine et d'Amérique du Sud, le passé colonialiste de la France d'où le fait qu'encore aujourd'hui, à l'heure actuelle, La Nouvelle-Orléans est la ville qui parle encore français, il y a également des immigrants Japonais ou des travailleurs qui ont les papiers en règle.

[Langues parlées aux Etats-Unis et leurs pourcentages jusqu&#39;en 2016](https://fr.wikipedia.org/wiki/Langues_aux_%C3%89tats-Unis)(https://fr.wikipedia.org/wiki/Langues_aux_%C3%89tats-Unis)

```


WITH LanguagePercentages AS (
    SELECT
        cl.Language,
        ROUND(cl.Percentage / SUM(cl.Percentage) OVER () * 100, 2) AS NormalizedPercentage
    FROM
        countrylanguage cl
    JOIN
        country c ON cl.CountryCode = c.Code
    WHERE
        cl.CountryCode = 'USA'
)
SELECT *
FROM LanguagePercentages
ORDER BY NormalizedPercentage DESC;

SELECT
    Language,
    ROUND(NormalizedPercentage * 100 / SUM(NormalizedPercentage) OVER (), 2) AS AdjustedPercentage
FROM
    LanguagePercentages;

```

Pourcentage de langages aux Etats-Unis.

[La population des Etats-Unis et son évolution depuis les années 1960](https://perspective.usherbrooke.ca/bilan/servlet/BMTendanceStatPays?codePays=USA&codeStat=SP.POP.TOTL&codeTheme=1)

```
+------------+----------------------+
| English    |                88.05 |
| Spanish    |                 7.66 |
| French     |                 0.72 |
| German     |                 0.72 |
| Chinese    |                 0.61 |
| Italian    |                 0.61 |
| Tagalog    |                 0.41 |
| Korean     |                 0.31 |
| Polish     |                 0.31 |
| Japanese   |                 0.20 |
| Portuguese |                 0.20 |
| Vietnamese |                 0.20 |
+------------+----------------------+
```

#Question 13:

* Pareil aux UK.

```
WITH LanguagePercentages AS (
    SELECT
        cl.Language,
        ROUND(cl.Percentage / SUM(cl.Percentage) OVER () * 100, 2) AS NormalizedPercentage
    FROM
        countrylanguage cl
    JOIN
        country c ON cl.CountryCode = c.Code
    WHERE
        cl.CountryCode = 'GBR'
)
SELECT * FROM LanguagePercentages;
SELECT
    Language,
    ROUND(NormalizedPercentage * 100 / SUM(NormalizedPercentage) OVER (), 2) AS AdjustedPercentage
FROM
    LanguagePercentages;

```

Pourcentage de langages parlés au Royaume-Uni.

[Langues parlées au Royaume-Uni en plus de l&#39;anglais](https://study-uk.britishcouncil.org/fr/pourquoi-etudier/a-propos-du-royaume-uni/langue)(https://study-uk.britishcouncil.org/fr/pourquoi-etudier/a-propos-du-royaume-uni/langue)

[l&#39;évolution de la population du Royaume-Uni depuis les années 1960](https://perspective.usherbrooke.ca/bilan/servlet/BMTendanceStatPays?codePays=GBR&codeStat=SP.POP.TOTL&codeTheme=1)

```
+----------+--------------------+
| Language | AdjustedPercentage |
+----------+--------------------+
| English  |              98.98 |
| Gaeli    |               0.10 |
| Kymri    |               0.92 |
+----------+--------------------+
```

#Question 14: Pour chaque région quelle est la langue la plus parler et quel pourcentage de la population la parle.

Le taux de language parlées qui sont les plus importantes selon les données actuelles. La Micronésie/Caraîbes qui a zéro de langues parlées c'est très étonnant mais des recherches s'imposent:

La Micronésie/ Caraïbes n'existe pas et aux Caraïbes ils parlent en première langue ou deuxième langue l'anglais:

[La principale langue parlée aux Caraïbes et dans les autres pays ou continents l&#39;utilisant](https://www.lingoda.com/fr/content/pays-anglophones/) (https://www.lingoda.com/fr/content/pays-anglophones/)

[La principale langue parlée en Micronésie et les autres langues parlées d&#39;un point de vue régional](https://www.langueofficielle.com/etats-federes-de-micronesie/)(https://www.langueofficielle.com/etats-federes-de-micronesie/)


```
SELECT
    Region,
    Language AS MostSpokenLanguage,
    PercentageOfPopulation
FROM (
    SELECT
        c.Region,
        cl.Language,
        MAX(cl.Percentage) AS PercentageOfPopulation,
        ROW_NUMBER() OVER (PARTITION BY c.Region ORDER BY MAX(cl.Percentage) DESC) AS rnk
    FROM
        countrylanguage cl
    JOIN
        country c ON cl.CountryCode = c.Code
    GROUP BY
        c.Region, cl.Language
) ranked
WHERE
    rnk = 1;

```

```
---------------------------+----------------------+------------------------+
| Region                    | MostSpokenLanguage   | PercentageOfPopulation |
+---------------------------+----------------------+------------------------+
| Australia and New Zealand | English              |                   87.0 |
| Baltic Countries          | Lithuanian           |                   81.6 |
| British Islands           | English              |                   98.4 |
| Caribbean                 | Haiti Creole         |                  100.0 |
| Central Africa            | Crioulo              |                   86.3 |
| Central America           | Spanish              |                  100.0 |
| Eastern Africa            | Rwanda               |                  100.0 |
| Eastern Asia              | Korean               |                   99.9 |
| Eastern Europe            | Hungarian            |                   98.5 |
| Melanesia                 | Malenasian Languages |                   85.6 |
| Micronesia                | Kiribati             |                   98.9 |
| Micronesia/Caribbean      | English              |                    0.0 |
| Middle East               | Arabic               |                   99.6 |
| Nordic Countries          | Faroese              |                  100.0 |
| North America             | English              |                  100.0 |
| Northern Africa           | Arabic               |                  100.0 |
| Polynesia                 | Tongan               |                   98.3 |
| South America             | Spanish              |                   99.0 |
| Southeast Asia            | Khmer                |                   88.6 |
| Southern Africa           | Swazi                |                   89.9 |
| Southern and Central Asia | Dhivehi              |                  100.0 |
| Southern Europe           | Italian              |                  100.0 |
| Western Africa            | Crioulo              |                  100.0 |
| Western Europe            | Dutch                |                   95.6 |
+---------------------------+----------------------+------------------------+
```

#Question 15:

* Est-ce que la somme des pourcentages de langues parlées dans un pays est égale à 100 ? Pourquoi ?

```
SELECT
    cl.CountryCode,
    ABS(SUM(cl.Percentage) - 100) AS DifferenceFrom100
FROM
    countrylanguage cl
GROUP BY
    cl.CountryCode
HAVING
    ABS(SUM(cl.Percentage) - 100) > 0.01; 

```

```
| CountryCode | DifferenceFrom100 |
+-------------+-------------------+
| ABW         |               1.1 |
| AFG         |               3.9 |
| AGO         |               4.6 |
| AIA         |             100.0 |
| ALB         |               0.2 |
| AND         |               6.1 |
| ANT         |               6.0 |
| ARE         |              58.0 |
| ARG         |               1.2 |
| ARM         |               4.0 |
| ASM         |               3.2 |
| ATG         |               4.3 |
| AUS         |              10.9 |
| AUT         |               2.9 |
| AZE         |               3.7 |
| BDI         |               1.9 |
| BEL         |               2.3 |
| BEN         |               7.2 |
| BFA         |              25.2 |
| BGD         |               1.3 |
| BGR         |               1.1 |
| BHR         |              32.3 |
| BIH         |               0.8 |
| BLR         |               0.5 |
| BLZ         |               1.2 |
| BOL         |               0.9 |
| BRA         |               1.0 |
| BRB         |               4.9 |
| BRN         |              13.3 |
| BWA         |               4.9 |
| CAF         |              17.6 |
| CAN         |               6.4 |
| CCK         |             100.0 |
| CHE         |               8.9 |
| CHN         |               1.3 |
| CIV         |              28.7 |
| CMR         |              19.3 |
| COD         |              19.7 |
| COG         |               9.5 |
| COK         |             100.0 |
| COL         |               0.3 |
| COM         |               4.5 |
| CXR         |             100.0 |
| CYM         |             100.0 |
| CYP         |               3.5 |
| CZE         |               0.8 |
| DEU         |               3.3 |
| DJI         |              10.7 |
| DNK         |               3.6 |
| EGY         |               1.2 |
| ERI         |               5.1 |
| ESP         |               0.7 |
| EST         |               2.0 |
| ETH         |              17.0 |
| FIN         |               1.0 |
| FJI         |               5.5 |
| FLK         |             100.0 |
| FRA         |               1.5 |
| FSM         |              10.2 |
| GAB         |              18.7 |
| GBR         |               1.7 |
| GEO         |               3.1 |
| GHA         |               7.5 |
| GIB         |               3.7 |
| GIN         |              11.4 |
| GLP         |               5.0 |
| GMB         |              20.3 |
| GNB         |              12.5 |
| GNQ         |               6.5 |
| GRC         |               0.6 |
| GTM         |               8.7 |
| GUF         |               3.8 |
| GUM         |               7.9 |
| HKG         |               4.2 |
| HND         |               1.1 |
| HRV         |               4.1 |
| HUN         |               0.2 |
| IDN         |              18.1 |
| IND         |               5.4 |
| IRN         |               7.4 |
| IRQ         |               0.5 |
| ISL         |               4.3 |
| ISR         |              10.0 |
| ITA         |               0.4 |
| JAM         |               3.9 |
| JOR         |               0.1 |
| KAZ         |               6.9 |
| KEN         |              11.1 |
| KGZ         |               5.4 |
| KHM         |               0.4 |
| KIR         |               0.6 |
| KWT         |              21.9 |
| LAO         |               3.3 |
| LBN         |               1.1 |
| LBR         |              24.7 |
| LBY         |               3.0 |
| LIE         |               6.0 |
| LKA         |               0.5 |
| LTU         |               0.8 |
| LUX         |              11.5 |
| LVA         |               2.1 |
| MAC         |              10.4 |
| MAR         |               2.0 |
| MCO         |              19.4 |
| MDA         |               1.5 |
| MDG         |               1.1 |
| MEX         |               3.4 |
| MHL         |               3.2 |
| MKD         |               2.3 |
| MLI         |              19.4 |
| MLT         |               2.1 |
| MMR         |               5.4 |
| MNG         |               7.6 |
| MNP         |              12.7 |
| MOZ         |              13.9 |
| MRT         |               1.2 |
| MSR         |             100.0 |
| MTQ         |               3.4 |
| MUS         |               2.2 |
| MWI         |               3.4 |
| MYS         |              23.2 |
| MYT         |              21.7 |
| NAM         |               2.2 |
| NCL         |               8.7 |
| NER         |               1.2 |
| NFK         |             100.0 |
| NGA         |               9.4 |
| NIC         |               0.1 |
| NIU         |             100.0 |
| NLD         |               1.0 |
| NOR         |               2.2 |
| NPL         |              13.2 |
| NRU         |               0.1 |
| NZL         |               8.7 |
| OMN         |              23.3 |
| PAK         |               2.9 |
| PAN         |               0.7 |
| PCN         |             100.0 |
| PER         |               1.5 |
| PHL         |              12.0 |
| PLW         |               3.8 |
| PNG         |               1.9 |
| PRI         |               1.3 |
| PRT         |               1.0 |
| PRY         |               0.7 |
| PYF         |               9.9 |
| QAT         |              59.3 |
| REU         |               1.5 |
| ROM         |               0.6 |
| RUS         |               4.4 |
| SAU         |               5.0 |
| SDN         |               9.2 |
| SEN         |               7.6 |
| SGP         |               1.4 |
| SHN         |             100.0 |
| SJM         |             100.0 |
| SLB         |               2.0 |
| SLE         |               5.6 |
| SOM         |               1.7 |
| SPM         |             100.0 |
| STP         |              13.0 |
| SUR         |              19.0 |
| SVK         |               0.5 |
| SVN         |               3.7 |
| SWE         |               4.9 |
| SWZ         |               8.1 |
| SYC         |               3.6 |
| SYR         |               1.0 |
| TCA         |             100.0 |
| TCD         |              11.4 |
| TGO         |              28.4 |
| THA         |               2.4 |
| TJK         |               4.9 |
| TKL         |             100.0 |
| TKM         |               5.4 |
| TMP         |             100.0 |
| TON         |               1.7 |
| TTO         |               0.2 |
| TUN         |               0.6 |
| TUR         |               0.4 |
| TWN         |               0.9 |
| TZA         |              24.5 |
| UGA         |              26.0 |
| UKR         |               0.7 |
| UMI         |             100.0 |
| URY         |               4.3 |
| USA         |               2.1 |
| UZB         |               4.5 |
| VAT         |             100.0 |
| VCT         |               0.9 |
| VEN         |               2.6 |
| VGB         |             100.0 |
| VIR         |               2.5 |
| VNM         |               2.8 |
| VUT         |               0.9 |
| WLF         |             100.0 |
| WSM         |               0.1 |
| YEM         |               0.4 |
| YUG         |               2.3 |
| ZAF         |               1.5 |
| ZMB         |              35.1 |
| ZWE         |               7.3 |
+-------------+-------------------+
```

En fonction des particularités par exemple la France, la population ne parle pas à 100% le français et il existe 75 langues locales en France dont le breton, le basque, le catalan etc.

[les langues locales parlées en France](https://fr.wikipedia.org/wiki/Langues_r%C3%A9gionales_ou_minoritaires_en_France)

[les langues parlées en Australie](https://fr.wikipedia.org/wiki/Langues_en_Australie)

[les langues parlées en Afrique du Sud](https://traduc.com/blog/langue-afrique-du-sud/)

[les langues parlées en Nouvelle-Zélande](https://www.langueofficielle.com/nouvelle-zelande/)

[les exemples de langues parlées à travers le monde et du pays ayant le plus grand nombre de langues parlées](https://altraductions.com/blog/les-pays-ayant-le-plus-grand-nombre-de-langues-officielles-dans-le-monde)

#Question 16:

Faire une carte du monde, avec une couleur par région. (chaque pays étant dans la bonne couleur).

Réponse dans le fichier main.py

Le fichier main.py permet de créer la carte du monde et de pouvoir visualiser les différentes régions et couleurs pouvant les différencier.

##### en conclusion

La base de données que nous avons traité est très intéressante et avec des recherches approfondies, nous pouvons voir aujourd'hui que le nombre de langues est très important avec la Bolivie qui a 32 langues officielles.

Le dernier lien de la question 15 permet de voir l'évolution des langues à travers le monde et à travers le monde, de voir qu'en France nous avons 75 langues régionales différentes par exemple.

[exemple pour la France](https://www.cia-france.fr/blog/culture-traditions-francaises/dialectes-francais)

[le nombre de langues parlées en Bolivie](https://fr.wikipedia.org/wiki/Langues_en_Bolivie)(https://fr.wikipedia.org/wiki/Langues_en_Bolivie)
