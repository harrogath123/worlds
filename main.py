import plotly.express as px
import mysql.connector
import pandas as pd
import sqlalchemy

engine = sqlalchemy.create_engine('mysql+mysqlconnector://david:coucou123@localhost/world')

# Requête SQL pour obtenir les données de la base de données
query = "SELECT Code, Region, Name FROM country ORDER BY Region ASC;"

# Charger les données dans un DataFrame
df = pd.read_sql(query, engine)

fig = px.choropleth(df, locations="Code",
                    color="Region",
                    hover_name="Name",
                    color_continuous_scale=px.colors.sequential.Plasma,
                    title="Distribution des pays par région")
fig.update_geos(projection_type="natural earth")  # Utiliser la projection naturelle de la Terre
fig.show() 
