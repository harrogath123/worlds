#Réponses

Réponse à la question une partie deux:

Pour chaque pays (avec au moins 1 client) le nombre de client, la langue
 officielle du pays (si plusieurs, peu importe laquelle), le CA total.

```
SELECT
    c.customerNumber,
    c.customerName,
    SUM(od.priceEach * od.quantityOrdered) AS totalRevenue
FROM
    customers AS c
JOIN
    orders AS o ON c.customerNumber = o.customerNumber
JOIN
    orderdetails AS od ON o.orderNumber = od.orderNumber
GROUP BY
    c.customerNumber
ORDER BY
    totalRevenue DESC
LIMIT
    10;

```

```|
+---------+-------------------+------------------+--------------+
| NULL    |                42 | NULL             |   3876849.00 |
| AUS     |                 5 | English          |    562582.59 |
| AUT     |                 2 | German           |    188540.06 |
| BEL     |                 2 | German           |    300206.28 |
| CAN     |                 3 | French           |    411823.72 |
| CHE     |                 1 | Romansh          |    435111.68 |
| DEU     |                 3 | German           |    196470.99 |
| DNK     |                 2 | Danish           |    218994.92 |
| ESP     |                 5 | Spanish          |   1099389.09 |
| FIN     |                 3 | Swedish          |    590298.70 |
| FRA     |                12 | French           |   1007374.02 |
| HKG     |                 1 | English          |     45480.79 |
| IRL     |                 1 | Irish            |     99796.54 |
| ITA     |                 4 | Italian          |    360616.81 |
| JPN     |                 2 | Japanese         |    167909.95 |
| NOR     |                 1 | Norwegian        |    104224.79 |
| NZL     |                 4 | English          |    476847.01 |
| PHL     |                 1 | Pilipino         |     87468.30 |
| SGP     |                 2 | Tamil            |    791993.34 |
| SWE     |                 2 | Swedish          |    187638.35 |
+---------+-------------------+------------------+--------------+
```

Réponse question deux partie deux:

La liste des 10 clients qui ont rapporté le plus gros CA, avec le CA correspondant.

```
SELECT
    c.customerNumber,
    c.customerName,
    SUM(od.priceEach * od.quantityOrdered) AS totalRevenue
FROM
    classicmodels.customers AS c
JOIN
    classicmodels.orders AS o ON c.customerNumber = o.customerNumber
JOIN
    classicmodels.orderdetails AS od ON o.orderNumber = od.orderNumber
GROUP BY
    c.customerNumber
ORDER BY
    totalRevenue DESC
LIMIT
    10;

```

```|
+----------------+------------------------------+--------------+
|            141 | Euro+ Shopping Channel       |    820689.54 |
|            124 | Mini Gifts Distributors Ltd. |    591827.34 |
|            114 | Australian Collectors, Co.   |    180585.07 |
|            151 | Muscle Machine Inc           |    177913.95 |
|            119 | La Rochelle Gifts            |    158573.12 |
|            148 | Dragon Souveniers, Ltd.      |    156251.03 |
|            323 | Down Under Souveniers, Inc   |    154622.08 |
|            131 | Land of Toys Inc.            |    149085.15 |
|            187 | AV Stores, Co.               |    148410.09 |
|            450 | The Sharp Gifts Warehouse    |    143536.27 |
+----------------+------------------------------+--------------+
```

Réponse question trois partie deux:

La durée moyenne (en jours) entre les dates de commandes et les dates d’expédition (de la même commande)

```
SELECT AVG(DATEDIFF(shippedDate, orderDate)) AS DureeMoyenne
FROM classicmodels.orders
WHERE shippedDate IS NOT NULL;

```

```
+--------------+
| DureeMoyenne |
+--------------+
|       3.7564 |
+--------------+
```

Réponse question quatre partie deux:

Les 10 produits les plus vendus.

```
SELECT
    p.productCode,
    p.productName,
    SUM(od.quantityOrdered) AS totalQuantitySold
FROM
    classicmodels.products p
JOIN
    classicmodels.orderdetails od ON p.productCode = od.productCode
GROUP BY
    p.productCode, p.productName
ORDER BY
    totalQuantitySold DESC
LIMIT 10;

```

```
+-------------+-----------------------------------------+-------------------+
| productCode | productName                             | totalQuantitySold |
+-------------+-----------------------------------------+-------------------+
| S18_3232    | 1992 Ferrari 360 Spider red             |              1808 |
| S18_1342    | 1937 Lincoln Berline                    |              1111 |
| S700_4002   | American Airlines: MD-11S               |              1085 |
| S18_3856    | 1941 Chevrolet Special Deluxe Cabriolet |              1076 |
| S50_1341    | 1930 Buick Marquette Phaeton            |              1074 |
| S18_4600    | 1940s Ford truck                        |              1061 |
| S10_1678    | 1969 Harley Davidson Ultimate Chopper   |              1057 |
| S12_4473    | 1957 Chevy Pickup                       |              1056 |
| S18_2319    | 1964 Mercedes Tour Bus                  |              1053 |
| S24_3856    | 1956 Porsche 356A Coupe                 |              1052 |
```

Réponse question six partie deux:

Le produit qui a rapporté le plus de bénéfice.

```
SELECT p.productName, SUM((od.priceEach - p.buyPrice) * od.quantityOrdered) AS benef
FROM classicmodels.orderdetails AS od
JOIN classicmodels.products AS p ON p.productCode = od.productCode
GROUP BY p.productCode
ORDER BY benef DESC LIMIT 1;

```

```
+-----------------------------+-----------+
| productName                 | benef     |
+-----------------------------+-----------+
| 1992 Ferrari 360 Spider red | 135996.78 |
+-----------------------------+-----------+
```

Réponse question sept partie deux:

```
SELECT
    p.productName,
    AVG(p.MSRP - od.priceEach) AS avgdif
FROM
    classicmodels.products AS p
JOIN
    classicmodels.orderdetails AS od ON p.productCode = od.productCode
GROUP BY
    p.productCode
ORDER BY
    avgdif DESC;

```

```
+---------------------------------------------+-----------+
| productName                                 | avgdif    |
+---------------------------------------------+-----------+
| 1968 Ford Mustang                           | 22.123704 |
| 2003 Harley-Davidson Eagle Drag Bike        | 21.371786 |
| 2001 Ferrari Enzo                           | 20.703333 |
| 1993 Mazda RX-7                             | 20.234444 |
| 1998 Chrysler Plymouth Prowler              | 19.587143 |
| 1957 Corvette Convertible                   | 19.509259 |
| 1928 Mercedes-Benz SSK                      | 18.862500 |
| 1980s Black Hawk Helicopter                 | 18.584643 |
| 2002 Suzuki XREO                            | 18.451071 |
| 1992 Ferrari 360 Spider red                 | 16.997170 |
| 1952 Alpine Renault 1300                    | 16.990714 |
| 1917 Grand Touring Sedan                    | 16.320000 |
| 1962 LanciaA Delta 16V                      | 15.933929 |
| 1969 Ford Falcon                            | 14.801111 |
| 1957 Chevy Pickup                           | 14.597857 |
| 1970 Triumph Spitfire                       | 14.573333 |
| 1976 Ford Gran Torino                       | 14.482222 |
| 1999 Indy 500 Monte Carlo SS                | 13.622400 |
| 1958 Setra Bus                              | 13.325000 |
| 1969 Corvair Monza                          | 13.316296 |
| 1940s Ford truck                            | 13.102857 |
| ATA: B757-300                               | 13.093929 |
| 1932 Model A Ford J-Coupe                   | 12.893929 |
| 1995 Honda Civic                            | 12.853333 |
| 1965 Aston Martin DB5                       | 12.742400 |
| 1962 Volkswagen Microbus                    | 12.505357 |
| 1948 Porsche Type 356 Roadster              | 12.206400 |
| 1956 Porsche 356A Coupe                     | 12.170741 |
| 18th century schooner                       | 11.970370 |
| Diamond T620 Semi-Skirted Tanker            | 11.946071 |
| 1972 Alfa Romeo GTA                         | 11.754286 |
| 1982 Camaro Z28                             | 11.741071 |
| 1964 Mercedes Tour Bus                      | 11.570714 |
| 1969 Dodge Charger                          | 11.345185 |
| 1928 British Royal Navy Airplane            | 11.332143 |
| 1997 BMW F650 ST                            | 11.202500 |
| 1940 Ford Pickup Truck                      | 11.166786 |
| The Queen Mary                              | 11.107778 |
| 1917 Maxwell Touring Car                    | 10.913571 |
| 1903 Ford Model A                           | 10.875926 |
| Collectable Wooden Train                    | 10.831111 |
| 1968 Dodge Charger                          | 10.787037 |
| 1997 BMW R 1100 S                           | 10.746429 |
| The Titanic                                 | 10.647778 |
| 1937 Lincoln Berline                        | 10.640714 |
| 1992 Porsche Cayenne Turbo Silver           | 10.600741 |
| 18th Century Vintage Horse Carriage         | 10.546071 |
| 1969 Harley Davidson Ultimate Chopper       | 10.525714 |
| 1980’s GM Manhattan Express                 | 10.456786 |
| 1949 Jaguar XK 120                          | 10.141600 |
| 1912 Ford Model T Delivery Wagon            | 10.130741 |
| 1941 Chevrolet Special Deluxe Cabriolet     | 10.095357 |
| 1960 BSA Gold Star DBD34                    | 10.010000 |
| 1969 Dodge Super Bee                        |  9.590000 |
| 1999 Yamaha Speed Boat                      |  9.490714 |
| 2002 Chevy Corvette                         |  9.465200 |
| 1952 Citroen-15CV                           |  9.444583 |
| 1904 Buick Runabout                         |  9.426667 |
| America West Airlines B757-200              |  9.366786 |
| 1932 Alfa Romeo 8C2300 Spider Sport         |  9.054000 |
| HMS Bounty                                  |  9.053214 |
| P-51-D Mustang                              |  9.020714 |
| 1970 Plymouth Hemi Cuda                     |  9.014074 |
| The Mayflower                               |  8.949259 |
| 1996 Moto Guzzi 1100i                       |  8.921071 |
| 1974 Ducati 350 Mk3 Desmo                   |  8.578519 |
| Corsair F4U ( Bird Cage)                    |  8.529286 |
| American Airlines: B767-300                 |  8.481429 |
| 1948 Porsche 356-A Roadster                 |  8.470000 |
| 1969 Chevrolet Camaro Z28                   |  8.322000 |
| American Airlines: MD-11S                   |  8.062143 |
| 1936 Harley Davidson El Knucklehead         |  7.960357 |
| 1982 Ducati 900 Monster                     |  7.901111 |
| 1913 Ford Model T Speedster                 |  7.886786 |
| 1936 Chrysler Airflow                       |  7.652857 |
| 1961 Chevrolet Impala                       |  7.574444 |
| 1971 Alpine Renault 1600s                   |  7.097407 |
| 2002 Yamaha YZR M1                          |  6.990741 |
| F/A 18 Hornet 1/72                          |  6.971429 |
| 1900s Vintage Bi-Plane                      |  6.853214 |
| The USS Constitution Ship                   |  6.852593 |
| 1970 Chevy Chevelle SS 454                  |  6.818000 |
| 1934 Ford V8 Coupe                          |  6.714643 |
| 1940 Ford Delivery Sedan                    |  6.678571 |
| 1928 Ford Phaeton Deluxe                    |  6.608571 |
| 1970 Dodge Coronet                          |  6.564286 |
| 1996 Peterbilt 379 Stake Bed with Outrigger |  6.511071 |
| 1911 Ford Town Car                          |  6.247200 |
| The Schooner Bluenose                       |  6.124815 |
| 1936 Mercedes-Benz 500K Special Roadster    |  6.046071 |
| 1939 Cadillac Limousine                     |  6.001071 |
| 1957 Vespa GS150                            |  5.918519 |
| 1957 Ford Thunderbird                       |  5.880000 |
| 1937 Horch 930V Limousine                   |  5.681429 |
| 1954 Greyhound Scenicruiser                 |  5.391429 |
| Boeing X-32A JSF                            |  5.232857 |
| Pont Yacht                                  |  5.197407 |
| 1930 Buick Marquette Phaeton                |  4.848929 |
| 1962 City of Detroit Streetcar              |  4.773333 |
| 1900s Vintage Tri-Plane                     |  4.735000 |
| 1926 Ford Fire Engine                       |  4.644286 |
| 1936 Mercedes Benz 500k Roadster            |  4.642400 |
| 1966 Shelby Cobra 427 S/C                   |  4.588000 |
| 1938 Cadillac V-16 Presidential Limousine   |  4.543571 |
| 1950's Chicago Surface Lines Streetcar      |  4.534074 |
| 1982 Lamborghini Diablo                     |  3.817407 |
| 1982 Ducati 996 R                           |  3.635926 |
| 1958 Chevy Corvette Limited Edition         |  3.296429 |
| 1939 Chevrolet Deluxe Coupe                 |  3.260000 |
+---------------------------------------------+-----------+
```
