USE classicmodels;

ALTER TABLE classicmodels.offices
ADD COLUMN country CHAR(30); -- Remplacez CHAR(10) par le type de données approprié

-- Remplacement du nom du pays par la clé primaire de world.country dans la table offices
UPDATE offices o
JOIN world.country w ON o.country = w.Name
SET o.country = w.Code;

-- Remplacement du nom du pays par la clé primaire de world.country dans la table customers
UPDATE customers c
JOIN world.country w ON c.country = w.Name
SET c.country = w.Code;

-- Ajout de la colonne temp_country dans classicmodels.customers
ALTER TABLE classicmodels.customers
ADD COLUMN temp_country CHAR(10); -- Remplacez CHAR(10) par le type de données approprié

-- Mettre à jour avec la langue pour tous les pays
UPDATE classicmodels.customers c
SET 
  c.temp_country = (SELECT w.Name FROM world.country w WHERE c.country = w.Name),
  c.language = (
    SELECT language 
    FROM world.countrylanguage 
    WHERE CountryCode = (SELECT w.Code FROM world.country w WHERE c.country = w.Name) 
    ORDER BY IsOfficial DESC, Percentage DESC LIMIT 1
  );

SELECT
    c.country,
    COUNT(DISTINCT c.customerNumber) AS `nombre de client`,
    GROUP_CONCAT(DISTINCT l.language) AS `langue`,
    SUM(od.priceEach * od.quantityOrdered) AS `CA`
FROM
    classicmodels.customers AS c
JOIN
    classicmodels.orders AS o ON c.customerNumber = o.customerNumber
JOIN
    classicmodels.orderdetails AS od ON od.orderNumber = o.orderNumber
JOIN
    world.countrylanguage AS l ON c.temp_country = l.CountryCode
GROUP BY
    c.country;




